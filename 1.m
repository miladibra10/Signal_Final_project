
% problem : different start time and finish time for phases


[y_no, fs_no] = audioread('no.mp3');
[y_yes, fs_yes] = audioread('yes.mp3');
[y_sample_yes, fs_sample_yes] = audioread('sample_yes.mp3');
[y_sample_no, fs_sample_no] = audioread('sample_no.mp3');

y_no = y_no(:,1);
y_yes = y_yes(:,1);
y_sample_yes = y_sample_yes(:, 1);
y_sample_no = y_sample_no(:, 1);

f_yes = fft(y_yes);
p_yes = angle(f_yes);
f_no = fft(y_no);
p_no = angle(f_no);

f_sample_yes = fft(y_sample_yes);
p_sample_yes = angle(f_sample_yes);
f_sample_no = fft(y_sample_no);
p_sample_no = angle(f_sample_no);

length = [size(p_yes, 1) size(p_no, 1) size(p_sample_yes, 1) size(p_sample_no, 1)];
m = min(length);

p_yes = p_yes(1:m, 1);
p_no = p_no(1:m, 1);
p_sample_yes = p_sample_yes(1:m, 1);
p_sample_no = p_sample_no(1:m, 1);

sample_yes_error_yes = sum((p_yes - p_sample_yes) .^ 2);
sample_yes_error_no = sum((p_no - p_sample_yes) .^ 2);

sample_no_error_yes = sum((p_yes - p_sample_no) .^ 2);
sample_no_error_no = sum((p_no - p_sample_no) .^ 2);

"predicted for YES sample"
if sample_yes_error_yes < sample_yes_error_no
    "yes"
else
    "no"
end

"predicted for NO sample"
if sample_no_error_yes < sample_no_error_no
    "yes"
else
    "no"
end
